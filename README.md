# README 
 
This is an exercise from the B9lab Certified Online Ethereum Developer Course, built mainly with Solidity and Javascript. 

The requirements are: 
 
* as an administrator, you can add products, which consist of an id, a price and a stock. 
* as a regular user you can buy 1 of the products. 
* as the owner you can make payments or withdraw value from the contract. 
* ability to remove products. 
* co-purchase by different people. 
* add merchants akin to what Amazon has become. 
* add the ability to pay with a third-party token.

I stopped working on this to work on the exam.