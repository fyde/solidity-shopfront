pragma solidity ^0.4.2;

contract Shopfront {

	struct Product {
		string name;
		uint price;
		uint stock;
	}

	mapping(uint => Product) public products;
	uint[] public ids;

	address public owner;
	address public admin;

	modifier onlyOwner() { if (msg.sender != owner) throw; _; }

	modifier onlyAdmin() { if (msg.sender != admin) throw; _; }

	function Shopfront() {
		owner = msg.sender;
		admin = msg.sender;
		//Temporary fix. Can't delete the key 0 in array, so we're not using it.s
		ids.push(0);
	}

	function idsLength() returns (uint length) {
		return ids.length;
	}

	function addProduct(uint id, string name, uint price, uint stock) onlyAdmin returns (bool successful) {
		products[id] = Product({
			name: name,
			price: price,
			stock: stock
		});
		ids.push(id);
		return true;
	}

	function removeProduct(uint id, uint key) onlyAdmin returns (bool successful) {
		delete products[id];
		delete ids[key];
		return true;
	}

	function killMe() onlyOwner returns (bool successful) {
		suicide(owner);
		return true;
	}
}
