module.exports = {
  build: {
    "index.html": "index.html",
    "app.js": [
      "javascripts/jquery-3.1.1.min.js",
      "javascripts/app.js"
    ],
    "app.css": [
      "stylesheets/app.css"
    ]
  },
  rpc: {
    host: "localhost",
    port: 8545
  }
};
