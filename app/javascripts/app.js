var accounts;
var coinbase;
var messages = [];
var products = [];
var productscount = 0;

//From B9lab courseware
function getTransactionReceiptMined(txnHash, interval) {
    var transactionReceiptAsync;
    interval |= 500;
    transactionReceiptAsync = function(txnHash, resolve, reject) {
        try {
            var receipt = web3.eth.getTransactionReceipt(txnHash);
            if (receipt === null) {
                setTimeout(function () {
                    transactionReceiptAsync(txnHash, resolve, reject);
                }, interval);
            } else {
                resolve(receipt);
            }
        } catch(e) {
            reject(e);
        }
    };

    return new Promise(function (resolve, reject) {
        transactionReceiptAsync(txnHash, resolve, reject);
    });
}

function setStatus(message) {
	html = "";
	messages.forEach(function(current) {
		html += "<div>" + current + "</div>";
	});
	if (messages.length > 5) messages.pop();
	html += "<div id='lastmessage'>" + message + "</div>";
	messages.push(message);
	document.getElementById('status').innerHTML = html;
}

//Function for async loop
var asyncLoop = function(o){
    var i=-1;
    var loop = function(){
        i++;
        if(i==o.length){
			if (typeof o.callback == 'function') {
				o.callback();
			}
			return;
		}
        o.functionToLoop(loop, i);
    }
    loop();
}

function addProduct(id, name, price, stock) {
	shop.addProduct(id, name, price, stock, {from: coinbase, gas: 3000000})
		.then(function (tx) {
			setStatus("Adding product...");
			return getTransactionReceiptMined(tx);
		})
		.then(function (receipt) {
			setStatus("Product added.");
			$('#addProduct_form').find("input[type=text], textarea").val("");
		});

}

function removeProduct(idtoremove) {
	var keytoremove;
	new Promise(function(fulfill, reject) {
		//Get products count
		shop.idsLength.call().then(function(count) {
			if (count.valueOf() > 0) {
				asyncLoop({
				    length : count.valueOf(),
				    functionToLoop : function(loop, i){
						shop.ids(i).then(function(id) {
							if (id == idtoremove) {
								keytoremove = i;
								fulfill();
							} else {
								loop();
							}
						}).catch(function (e) {
							setStatus("Error: " + e);
							reject();
						});
				    }
				});
			}
		});
	}).then(function() {
		if (keytoremove == 0) {
			return false;
		}
		shop.removeProduct(idtoremove, keytoremove, {from: coinbase, gas: 3000000})
			.then(function(tx) {
				setStatus("Removing product...");
				return getTransactionReceiptMined(tx);
			})
			.then(function (receipt) {
				setStatus("Product removed.");
				return true;
			})
	});
}

function listProducts() {
	$.each(products, function(key, value) {
		if (typeof value !== 'undefined') {
			$("#productlist table").append("<tr><td>" + key +
											"</td><td>" + value[0] +
											"</td><td>" + value[1] +
											"</td><td>" + value[2] +
											"</td><td>" +
											"<button onclick='removeProduct(" + key + ")' type='button'>Remove</button>" +
											"</td></tr>");
		}
	});
	$("#loadingProducts").toggle();
}

function getNextId() {
	highestid = 0;
	shop.idsLength.call().then(function(count) {
		if (count.valueOf() > 0) {
			asyncLoop({
				length : count.valueOf(),
				functionToLoop : function(loop, i){
					shop.ids(i).then(function(id) {
						if (id > highestid) {
							highestid = id;
						}
						loop();
					}).catch(function (e) {
						setStatus("Error: " + e);
						reject();
					});
				},
			    callback : function(){
					$('input[name=add_id]').val(parseInt(highestid)+1);
				}
			});
		}
	});
}

window.onload = function() {
	$('#addproduct_toggle').click(function() {
		$('#addProduct_div').slideToggle();
	});

	$('#productlist_toggle').click(function() {
		$('#productlist').slideToggle();
	});

	web3.eth.getAccounts(function(err, accounts) {
		if (err !== null) {
			setStatus("There was an error fetching your accounts.");
			return;
		}

		if (accounts.length === 0) {
			setStatus("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
			return;
		}

		setStatus("Accounts fetched.");

		coinbase = accounts[0];
		shop = Shopfront.deployed();

		//Get products
		new Promise(function(fulfill, reject) {
			//Get products count (included the ones deleted)
			shop.idsLength.call().then(function(count) {
				if (count.valueOf() > 0) {
					//Get every product from the products array using the ids
					asyncLoop({
					    length : count.valueOf(),
					    functionToLoop : function(loop, i){
							shop.ids(i).then(function(id) {
								//If id is 0 the product has been deleted
								if (id == 0) {
									productscount++;
									//Check if we got everything and fullfil the promise
									if (count.valueOf() == productscount) {
										fulfill();
									};
									loop();
								//Else the product is there, get it
								} else {
									shop.products(id.valueOf()).then(function(values) {
										//Put the product in the array
										products[id] = [values[0], values[1], values[2]];
										productscount++;
										//Check if we got everything and fullfil the promise
										if (count.valueOf() == productscount) {
											fulfill();
										};
										loop();
									}).catch(function (e) {
										setStatus("Error: " + e);
										reject();
									});
								}
							}).catch(function (e) {
								setStatus("Error: " + e);
								reject();
							});
					    }
					});
				}
			});
		}).then(function() {
			listProducts();
		})

	});
};
